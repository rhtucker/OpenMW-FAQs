# What is OpenMW/OpenMW-CS?

OpenMW is a free, open source and modern engine based which reimplements and extends the one that runs the 2002 open-world RPG Morrowind. The engine comes with its own editor , called OpenMW-CS which allows the user to edit or create their own original games. Both OpenMW-CS and OpenMW are written from scratch and aren’t made to support any third party programs the original Morrowind engine uses to improve its functionality.

NOTE: Playing Morrowind with this engine still requires the Morrowind data files that you own.

To give you a better idea of what this project is about, here are some of the goals for OpenMW and OpenMW-CS.

Version 1.0 Goals:

* Be a full-featured reimplementation of the Morrowind engine.
* Be able to create and build your own game from scratch, free of Bethesda’s intellectual property (Morrowind).
* Run natively on Windows, Linux and macOS.
* Support all existing content, including Tribunal, Bloodmoon and all user created mods that do not use use external programs and libraries.
* Fix system design bugs, like save-game “doubling” problem.
* Improve the interface and journal system.
* Improved graphics by taking advantage of more modern hardware.
* Improve game physics and AI.

Post 1.0 Wishlist:

* Additional platform support: Android, FreeBSD (and variants), iOS, Raspberry Pi and more.
* Allow much greater modability: change game rules, create new spell effects, etc. through scripting.
* Debugging support via OpenMW-CS for content.
* Plugins for OpenMW-CS, such as Terrain import, export and generation.
* Improved game mechanics and combat.
* Improved the interface and journal system.
* Multiplayer support, at least some form of co-op.

NOTE: The above wishlist indicates the willingness of the OpenMW devs to work on these features, but are currently not high on the priority list unlike the version 1.0 goals.

OpenMW-CS will support the editing of all OpenMW features. We aim for the editor to stay fully up-to-date with the corresponding OpenMW version, allowing the user to edit any newly implemented features. The OpenMW-CS is code-wise not based on the editing tool (CS) which came with the original Morrowind game, but it does improve the later’s good points, drops the bad and ugly points and adds additional features and tools that makes it more a tool made by modders for modders.

Important properties of the OpenMW-CS are:

* non-blocking
* multi-threaded
* multi-document support
* multi-view support
* high scalability
* customisable GUI

OpenMW/OpenMW-CS is released under the GNU General Public License version 3, and all source code has been written completely from scratch.

Zini, the project lead for OpenMW, had this to say about Version 1.0:

>>>
Since the 1.0 topic has come up recently a couple of times, I think I should elaborate on it a bit.

The goal for OpenMW 1.0 is be a complete replacement of Morrowind.exe. Nothing more, nothing less. That also means that improvements over the original game are mostly ruled out for now. I pushed for this direction, because it is the safest way to finish OpenMW 1.0 as quickly as possible.
We consider this a very important milestone, because with 1.0, OpenMW will change from a demo to an actually playable game. We hope OpenMW 1.0 will draw a lot of attention; in the form of new developers and even more importantly a massive amount of new testers.

There are situations where we can diverge from Morrowind, even prior to 1.0.

First I don’t feel any obligation to copy any non-functional behaviour. There is absolutely no point to mimic a crash or any other kind of failure mode. There is also no point in mimicing limits (like the if-then nesting depth limits in scripts), if an implementation without these limits isn’t more work.
Generally with anything that does not work at all in Morrowind we have complete freedom in deciding how to handle it. Note that this does not include any bad game mechanics. Bad is not equal disfunctional.

Second, we can change anything that is not part of the in-game experience, e.g. the commandline options and the configuration files for OpenMW look completely different from what Morrowind is using. The launcher is another example for this case.

Third, we are not obliged to choose the same implementation path as Morrowind (where we know or can guess what MW is doing). It still should look the same to the player, but what is under the hood can look completely different.

And finally fourth, there are a few edge cases where we actually might diverge slightly from Morrowind on purpose, when it makes the implementation a lot easier or a perfect copy is a clear no go, because something has absolutely no future and a perfect copy would mean we would have to rip out the whole thing after 1.0 and reimplement the part from scratch.

Zini
>>>

# What can you expect from the current release?

OpenMW is pre-v1.0 and under active development. This means that the performance of the engine isn’t as it should be, that there are still multiple missing features and that bugs are to be expected. Can you hunt them all down?

# Who is working on OpenMW/OpenMW-CS?

See the “The Team”  page!

# Do I need Morrowind to use OpenMW?

Yes, if you wish to play Morrowind and its expansions. You must legally own Morrowind before you can use OpenMW to play Morrowind. OpenMW is a game engine recreation and only replaces the program. OpenMW does not come with any “content” or “asset” – namely the art, game data, and other copyrighted material that you need to play the game as designed by Bethesda Softworks. You have to provide this content yourself by installing Morrowind and then configuring OpenMW to use the existing installation.

If you don’t currently own Morrowind, seriously consider buying it! It’s worth every penny, even if it’s no longer a “new” game. You can buy Morrowind (GOTY edition) online from the usual places (sometimes in sales):

* GOG
* Steam

No, OpenMW is a complete game engine. It is possible for other projects to use OpenMW and OpenMW-CS to create their own game game. There are several projects currently underway such as:

OpenMW-Template: A bare-bone template “game” with everything necessary to run OpenMW. It can be extended, improved and further developed using OpenMW-CS.

OpenMW-Example-Suite: This uses the OpenMW-Template as a starting part and is OpenMW’s first official “game”, used to show off the OpenMW engine and give content creators an idea on what they can do with the engine.

# What technologies does OpenMW/OpenMW-CS use?

OpenMW is built with various open source tools and libraries:

* Programming language: C++
* Graphics: OpenSceneGraph (OSG)
* Physics: Bullet
* Sound: OpenAL
* GUI: MyGUI
* Input: SDL2
* OpenMW-Launcher and OpenMW-CS both use Qt for their GUI.

Morrowind’s scripting engine was reimplemented and improved.

The ESM/ESP and BSA loading code was written from scratch, but with much help from available community-generated documentation.

Likewise, the NIF (proprietary 3D mesh) loading code was written with the help of available online information. Special thanks to the NIFLA / NifTools gang!

(For an additional list of the technologies in use, see Development Environment Setup: Third-Party Libraries and Tools.)

# Will the main branch of OpenMW support multiplayer?

OpenMW contributors are currently directing all of their focus towards the project’s version 1.0 goals, which do not include multiplayer. However, another project called TES3MP has forked the main project’s code in order to implement multiplayer, and is keeping up to date with changes in the official OpenMW branch. It is conceivable that the multiplayer fork could be merged back into the main branch when both projects are in a suitable state for it. This would allow for a single app that can run both singleplayer and multiplayer Morrowind.

# I can launch OpenMW and music plays, but I only see a black screen!

This can occur when you did not import the Morrowind.ini file when the launcher asked for it.
To fix it, you need to delete the launcher.cfg file from your configuration path (Path description on Wiki), then start the launcher, select your Morrowind installation, and when the launcher asks whether the Morrowind.ini file should be imported, make sure to select Yes.

# Can I use the assets from the Xbox or Xbox 360 version of Morrowind with OpenMW?

No, there is currently no support for the Xbox version of Morrowind. Bethesda has asked us not to mix platforms in this regard, so there will be no Xbox Morrowind on the PC or any other non-Xbox platform, except for the Xbox itself and the Xbox 360.

# What is the “official” status of OpenMW on alternative platforms?

The primary target platforms will always be GNU/Linux, macOS, and Windows. There are however various stages of support for Android, FreeBSD (and variants), Raspberry Pi and others. They will likely never be “first class citizens” unless more than one developer is actively working to maintain support.

# What can you do with OpenMW on Android or Raspberry Pi?

You can run the OpenMW-Template right from the start on devices that support at least OpenGL 2.0 with ARVv7 and above.

This means OpenMW can run on a Raspberry Pi 2 provided that VC4 kernel module and userland support is there. It should be mainlined into Linux kernel 4.5 and available in Xorg as of 11.1. OpenMW will compile on Raspberry Pi B+ or earlier, getting it running will be more difficult. Feel free to experiment.

# Can I play Morrowind with OpenMW on other platforms like the Raspberry Pi?

It depends entirely on if your device’s OpenGL software and hardware supports the S3TC flag. If it doesn’t, the textures won’t show up so you’ll just see pink “filler” textures. This is because the textures shipped with Morrowind are compressed with S3TC, a patented texture compression algorithm. You, as the end-user, have to decide if you are legally able to decompress the textures yourself before using OpenMW to play Morrowind.

The Raspberry Pi’s VC4 hardware doesn’t support S3TC. Your millage will vary with Android as the GPU can vary drastically across devices.

# When does the S3TC patent expire, when it does can we play Morrowind then?

The patent expires on “Oct 2, 2017” but it can be renewed before then. If it does expire, then OpenMW will feel comfortable enough to allow code into OpenMW for decoding/decompressing textures on the fly for systems that do not support it, such as the Raspberry Pi.

# Didn’t Bethesda have a problem with non-PC ports of Morrowind, won’t you guys have legal problems with this port?

We had a long email conversation with Matt Grandstaff which can be read on the wiki. There was a misunderstanding as to what OpenMW is (a new game engine) and isn’t (a Morrowind port) that was cleared up. We came to an understanding that we, OpenMW, would not promote any images or videos of OpenMW running Morrowind on the Android. Anything else, is fair game, such as: displaying videos of OpenMW running the OpenMW-Template on Android on the blog.

We do not condone nor are responsible for other websites that show (in images or video) Morrowind running on an Android device.

# How can I help?

If you have suggestions, ideas, comments, or if you want to contribute code, you are very welcome to join the official OpenMW forum, and visit our #openmw IRC channel at FreeNode.

You can also check out our Issue Tracker!

# Do you accept donations? Where can I send the money?

OpenMW itself won’t accept accept monetary donations.  The best donation a non-developer can give is testing and feedback.

However, there are individual developers that would be happy to spend more time on improving open source software and libraries used by OpenMW such as OpenSceneGraph, OpenAL-Soft and MyGUI. If you wish to support them you can donate. Send the donation directly to them and it would benefit OpenMW as well as all the other projects they are contributing to.

An example of this kind of developer is scrawl who accepts donations through his website.

# License and legal stuff

This website, its author, and OpenMW are not in any way associated with or supported by Bethesda Softworks or ZeniMax Media Inc. OpenMW is a completely hobbyist project.

This website does NOT distribute any game data or other copyrighted content not owned by the author. You MUST own a legal copy of Morrowind before you can use OpenMW to play Morrowind. We absolutely do NOT support nor condone pirated versions of the game. All source code is written from scratch and is released under the GNU General Public License version 3.

All fonts, shaders and other assets shipped with OpenMW are licensed under separate terms. Details can be found in the Readme file or in the Wiki.

Morrowind, Tribunal, Bloodmoon, The Elder Scrolls, Bethesda Softworks, ZeniMax and their respective logos are registered trademarks of ZeniMax Media Inc. All Rights Reserved. All other trademarks are properties of their respective owners.

All textures, models, designs, sounds and music reproduced in screenshots and videos are the property of ZeniMax Media Inc. unless otherwise specified.