%--------------------------------------%
%----- Part 1: The OpenMW Project -----%
%--------------------------------------%

%
%--- Intro
%

Hello, and welcome to our new FAQ video series which will replace the rather outdated FAQ video released in September 2012. Since then, the OpenMW project has come a long way. Our engine is nearly feature-complete by now, and the developers are concentrating on bug-fixing, fine-tuning, and implementing the last missing features.

The goals of this series are to properly introduce our project, answer the most common questions about OpenMW, and give some in-depth information about certain aspects of the engine, e.g., the current development status or future moddability.

In the first video -- which you are watching now -- I'll give a general introduction to the project and explain the advantages OpenMW brings for our different target audiences. The second video will summarise the project's current status and the remaining tasks for version 1.0. In the third video we will take a closer look at OpenMW's future by discussing the engine's potential and naming limitations to both the OpenMW project as well as the engine itself. The fourth video will focus on compatibility issues with the original game OpenMW was designed to run in the first place, mainly speaking of problems with user-created modifications to the vanilla game. The fifth video will be a call to arms for everyone who is interested in helping OpenMW to reach its goals -- whether you are an experienced coder, a fan of the original game, or an artist who creates models and textures.

Enough said. Now, let me introduce you to the OpenMW project.

%
%--- What exactly is OpenMW?
%

Q:
What exactly is OpenMW? X

A:
OpenMW is a free and open source software designed to run the classic RPG The Elder Scrolls III: Morrowind. The original game has been created by Bethesda Softworks and was released in 2002. It received critical acclaim because of its open-world character and its innovative setting. It came with its own editor, The Elder Scrolls Construction Set, which allowed players to modify many aspects of the game, leading to a huge amount of mods -- some of which are still being worked on.
	
Bethesda also created other famous RPGs, like parts four and five of the The Elder Scrolls series, Oblivion and Skyrim, as well as the revival of the Fallout series. The OpenMW team wants to build an engine called OpenMW to replace Morrowind's original engine, making the latter obsolete in the future.

%
%--- Why would you reimplement a whole game engine? X
%

Q:
Why would the OpenMW team start such a huge undertaking in the first place? Morrowind has been playable when it was released, and it is still running on recent Windows versions. X

A:
Well, Morrowind's original engine has its flaws. There are many bugs which will never be fixed considering the game's age. Moreover, it tends to be unstable and was optimised for, now, very outdated hardware. Also, the modding capabilities are limited when it comes to changes in the actual game mechanics, which lead to a variety of hackish third-party software to overcome at least some of the hard-coded limitations. Last but definitely not least, the vanilla engine only runs on Windows, ignoring a growing part of the gaming community.

%
%--- Isn't this illegal?
%

Q:
You are rebuilding a commercial game? Isn't this illegal? X

A:
No, the OpenMW project is a completely legitimate engine reimplementation. It is neither a modification nor a third-party engine hack. We are just writing a piece of software which is able to run Morrowind by reading its game files, and every part of the engine has been written from scratch. We do not provide any of the original contents and have obtained our information through clean reverse-engineering. The game files themselves are still property of Bethesda Softworks and ZeniMax Media.
 
%
%--- What are your goals for version 1.0?
%

Q:
Okay, I get it. What are your goals for version 1.0 then? X

A:
The OpenMW engine has a great potential which goes far beyond the scope of the original game. But for the time being, the OpenMW team concentrates on reimplementing Morrowind with all of its features, some of its bugs when necessary, and compatibility with all of its mods, given they have been created in a clean way.
	
We'll consider the engine ready for version 1.0 when the following goals have been reached:
	
	- OpenMW correctly implements every feature of the original game, while adjusting obviously wrong behaviour when it is reasonable.
	- OpenMW fixes every system design bug in the original engine, e.g., the infamous bloating of savegame files during a play-through, or the limit of 256 mods to be active at a time.
	- OpenMW supports all existing content, including the add-ons Tribunal and Bloodmoon as well as every mod which has been created without using third-party software or libraries.
	- OpenMW runs natively on Windows, Linux, and Mac OSX.
	- OpenMW takes advantage of modern hardware in order to achieve a better graphical quality without a performance hit.
	- OpenMW improves the interface and journal system of the original engine.
	- OpenMW improves the game's physics and AI.
	- OpenMW allows developers to create a new game from scratch, independent from Bethesda's contents.
	
Of course, these goals can be adjusted in case of unreasonable delays because of a single feature missing but we consider the first four goals essential for OpenMW 1.0.
 
%
%--- What are OpenMW's hardware requirements?
%

Q:
What are OpenMW's hardware requirements? Will OpenMW perform better than the original engine? X

A:
Morrowind is an old game and should run well on any remotely up-to-date system. Since version 0.37.0, our engine's performance equals or exceeds the vanilla one's in most situations. However, there are some restrictions when comparing both pieces of software. First of all, OpenMW has not been optimised yet, leaving room for noticeable improvements. Secondly, OpenMW ships with improved graphics which complicates a straight comparison between the engines. Finally, there are a few problems with certain system setups, especially with AMD drivers. To cut a long story short, if your PC is above Morrowind's hardware requirements, the game should perform well using OpenMW. If not, please contact us in our Support subforum at openmw.org.
 
%
%--- Do I need a copy of TES III: Morrowind in order to play OpenMW? X
%

Q:
You are replacing Morrowind's engine with a new one. Do I need the original game files in order to play Morrowind on OpenMW? X

A:
OpenMW only is an engine designed to run Morrowind's game files. We do not provide any of the original content -- which would, by the way, be a severe copyright infringement. So yes, you need the original game to run Morrowind using OpenMW. It is still available on Steam or GOG for a fair price, and, sometimes, you can still find it for sale at game stores.
	
Please note that you don't need Morrowind to run OpenMW. You only need the Morrowind game files, if you want to play Morrowind on OpenMW. This is especially true for new games created with our editor, OpenMW-CS.
 
%
%--- Is it possible to port my old Morrowind savegames to OpenMW?
%

Q:
Is it possible to port my old Morrowind savegames to OpenMW? X

A:
In general, no. There is an early version of a savegame importer but it is far from being finished. Maybe, someone is willing to continue work after version 1.0 has been released. We advise you to start a new game when switching over to OpenMW.
 
%
%--- I just want to play Morrowind. Why would I use OpenMW instead of the vanilla engine?
%

Q:
I'm just a Morrowind enthusiast who wants to enjoy the game without any modding magic. Why would I use OpenMW instead of the vanilla engine? X

A:
OpenMW is a modern engine and runs natively on Windows, Linux, and Mac OSX. And the best thing is, you can play the game on any screen resolution. OpenMW is actively maintained and fixes most of the vanilla engine's bugs and design flaws. We have reworked the savegame management and have improved the interface, e.g., by making all UI windows adjustable, redesigning the Alchemy window, or adding comfort functions to the mercantile window. The placement of items has seen improvements too. Moreover, a great amount of settings has been added, e.g., to adjust the view distance, make the sneak mode toggle, or show the owner status of items and activators.
 
%
%--- I play Morrowind using tons of mods, MWSE, MGE, and MGSO. Why would I switch to your engine?
%

Q:
I play Morrowind using tons of mods, MWSE, MGE, and MGSO. Why would I switch to your engine? X

A:
First of all, OpenMW should support every user-created content, if it doesn't rely on third-party software or engine hacks. Obviously, this doesn't include utilities like Morrowind Script Extender or Morrowind Graphics Extender . However, OpenMW will provide most of the additional functionalities right from the start in the future, compensating for the lack of compatibility. Modifications relying on third-party software may remain incompatible though. They need to be rewritten or, more likely, to be substituted with written-from-scratch modifications using OpenMW's extended modding capabilities.
	
So, if utilities like the Morrowind Graphics Extender are a must-have for your Morrowind play-throughs, OpenMW may not be the best choice for you right now as it will take some time after version 1.0 has been released until OpenMW features everything the great Morrowind modding community has invented to cope with the original engine's limitations.
 
%
%--- I want to mod Morrowind. What advantages does OpenMW provide for modders?
%

Q:
I want to mod Morrowind. What advantages does OpenMW provide for modders? X

A:
Morrowind came with its own editor, The Elder Scrolls Construction Set , which allowed modders to change many aspects of the game. However, there were many hard-coded limitations, and the editor always felt somewhat clunky.
	
The OpenMW team has created its own editor, OpenMW-CS, to overcome these restrictions. Our editor is multi-threaded, non-blocking, and provides a modern interface which is customisable and supports multiple views. Modders may open as many documents at the same time as they want to, while keeping the load on their system low. It is also possible to create new games without any Morrowind dependencies.
	
On the hard-coding side of things, the OpenMW team plans to get rid of most limitations after version 1.0 has been released. In the future, modders will be able to add new skills to the game, overhaul the combat system, create schedules for NPCs, and much, much more. And keep in mind that all of this will happen in a clean way, ensuring a high degree of compatibility and consistency.
 
%
%--- I want to develop a game based on the OpenMW engine. Is this possible, and am I allowed to sell my game in the end?
%

Q:
I want to develop a game based on the OpenMW engine. Is this possible, and am I allowed to sell my game in the end? X

A:
Like I said before, OpenMW-CS provides the option to create new games running on the OpenMW engine. We will also provide a game template which contains the minimum of required assets a developer would need in order to create a new game -- independent from Bethesda's contents, of course. The corresponding Game Template project as well as the OpenMW Example Suite whose purpose it is to showcase OpenMW's additional features are already in slow but active development.
	
Regarding commercialising your game: OpenMW is licensed under the GNU GPL license. This license grants free access to the OpenMW source code. If you develop a game and change the source code in the process, you must ensure public access to that particular code. If you don't change the source code at all, your customers can simply download OpenMW from official sources. Apart from the source code, any assets you have created for your game are under your copyright and are not subjected to the GNU GPL.
	
To sum things up: Yes, you can create a game with OpenMW-CS and sell it. You must provide free access to your source code though.
 
%
%--- Outro
%

That's all for the general introduction to OpenMW. The next part covers the project's current status. If my answers to the frequently asked questions raised even more questions, please leave a comment, or visit our forums at openmw.org. The corresponding links as wells as references to other relevant sites are available below this video.

See you in part two of the FAQ series. And, as always, thanks for watching!



***



%----------------------------------%
%----- Part 2: Project Status -----%
%----------------------------------%

%
%--- Intro
%

Welcome to the second part of our FAQ video series. This time, we will talk about the project's current status.

%
%--- You've just released version 0.40.0. Another 60 releases to go until version 1.0? X
%


Q:
So, you've just released version 0.40.0. Another 60 releases to go until version 1.0? X

A:
The OpenMW team uses the so-called Semantic Versioning to index OpenMW releases. A version label consists of three numbers, separated by a point. The first number refers to the current main version. Since OpenMW has not reached its first set of main goals, the current main version is 0. It will be increased to 1 as soon as we've reached all of the goals I've introduced in the first part of this series.
	
The second number indicates the minor version count. It is increased with every release, 40 of which we had until today. Accordingly, the number is by no means a percentage value of our progress. If you want to convert the current version into such a value, I'd say that we've released version 0.95 or 0.96. The minor version counter is reset to 0 when the major version counter is increased.
	
The last number represents maintenance releases or patches. These are small updates in case of severe errors popping up after a minor version's release. The patch counter is reset to 0 once another minor version release has arrived. Oh, and don't get confused: We often skip the last number because we are lazy as hell!

%
%--- Is Morrowind currently playable on OpenMW? X
%

Q:
Now, be honest: Is Morrowind currently playable on OpenMW? X
	
A:
The short answer is: Yes, Morrowind is already playable on OpenMW!
	
And here's the long answer: As far as we know, the main story, both add-ons as well as every side quest of the vanilla game are working. The same goes for nearly every game mechanic, apart from a few missing AI features. Testers constantly give us feedback about our engine's performance -- and current replies show that our engine often runs more stable than the original one, without noticeable bugs and with a decent frame rate, even on high graphics settings.
	
We get more and more reports about issues with mods though which is related to the increased number of players using OpenMW. Often, errors occur because of mod author's hackish solutions to the original engine's limitations, or because of plain scripting errors. However, there are lots of bugs or unexpected behaviours on OpenMW's side which only appear, if the engine gets stressed beyond the vanilla game's standards. We are also implementing a lot of fall-backs and workarounds to make OpenMW compatible to as many mods as possible.

%
%--- What exactly keeps you from releasing version 1.0?
%

Q:
What exactly keeps you from releasing version 1.0? X

A:
There are four main things which prevent us from directly jumping to version 1.0: the status of our editor, OpenMW-CS, missing features, bugs, and missing optimisation.
	
OpenMW comes with its own editor, OpenMW-CS. We initially planned to release OpenMW 1.0 and OpenMW-CS 1.0 at the same time in order to allow modders to create new content right away. However, the progress on our editor had been much slower for a long time which resulted in a big gap between OpenMW's playability and OpenMW-CS's usability. Because of that the OpenMW team has decided to uncouple both pieces of software and will release OpenMW version 1.0 even without the editor being fully functional.
	
When you have a look at our bug tracker, you get an impression of what's missing right now on OpenMW's side. First of all, there are a small number of features which need to be implemented. These include several AI-related tasks, e.g., a proper implementation of the AI's fleeing behaviour and an improved path-finding AI. A full list of the missing features is available in the video links below.
	
The OpenMW team wants to release a product which is as bug-free as possible. According to our bug tracker, the number of bugs which need to be fixed before version 1.0 can be released is in the double figures. More bugs are likely to show up in the future but we've managed to constantly decrease the number of remaining issues in the past. Nevertheless, bug-fixing is a tedious task which can be very time-consuming.
	
The last reason for delaying the 1.0 release is a rather vague yet complex one: optimisation. As I said earlier, OpenMW performs very well. There are a few weaknesses though, namely certain problems with the physics engine and performance problems on AMD graphics cards. Additionally, the OpenMW team not only wants to create an engine which is equal to a 14-year-old game engine but also allows the use of modern graphic features, more scripts, and more content, while keeping a decent frame rate. We will have to trade perfect optimisation off against a reasonable release date for version 1.0.

%
%--- OpenMW seems to be rather complete. When do you expect version 1.0 to arrive?
%

Q:
OpenMW seems to be rather complete. When do you expect version 1.0 to arrive? X

A:
OpenMW is an open source project, and every team member working on OpenMW does so on a voluntary basis. That said, we don't give exact release dates. But let me give an estimation instead: I would say that we will have two to three minor releases until version 1.0 is ready, i.e., 0.41, 0.42, maybe a 0.43 and, then, an OpenMW 1.0.0. Given an average time of three to four months per release and counting from September 2016, we end up with a 1.0 release between June 2017 and January 2018.

%
%--- Will there be a beta test or something like that?
%

Q:
Will there be a beta test or something like that? X

A:
There are supporters and opponents of a beta test -- and we had quite a few discussions about that topic. The first group argues that it would be beneficial to announce a public beta in order to make clear where the project stands and to draw the attention of additional testers. The latter group wants to avoid the term beta as OpenMW is playable right now, and only in alpha status because of some minor features missing. They opt for an extended test phase before the 1.0 release.
	
We will decide on that matter when the time has come -- and, of course, inform you about our decision. Despite the naming issues, you can assume that we will rather delay version 1.0 to thoroughly test OpenMW's functionality than rush an unstable OpenMW 1.0.

%
%--- Outro
%

Alright, any questions left? If yes, simply put them in the comments tab below, or head over to our forums at openmw.org. Below this video, you'll also find the most important links regarding our project.

See you in the next part which will cover OpenMW's potential and limitations. Until then, thanks for watching!



***



%--------------------------------------------------------%
%----- Part 3: The Engine's Potential & Limitations -----%
%--------------------------------------------------------%

%
%--- Intro
%

Hello, and welcome to the third part of our OpenMW FAQ series. After we have properly introduced the project itself and talked about the current status, we are prepared to have a look at OpenMW's future.

%
%--- Okay, let us assume that version 1.0 has been released. What now?
%

Q:
Okay, let us assume that version 1.0 has been released. What now? X

A:
Version 1.0 is going to be a milestone in OpenMW's development because, after its release, the OpenMW team isn't tied to reimplementing Morrowind and its mechanics anymore. Several team members have already stated what they want to do in order to further improve the engine's graphical power, the scripting system, the game mechanics system, the overall moddability, and the creation of entire new games.
	
Due to its open-source character, everyone is free to add or change pieces of our code -- and provide the updated code to others, or even merge it into the main OpenMW project. We don't expect the development activity to explode after the 1.0 release but the newly gained freedom should encourage more coders to become part of the project. Remember, nearly every feature is possible, if there's somebody willing to implement it.
	
The release procedure will stay the same after version 1.0 has been released: We will continue to develop our engine and decide to have a minor release when enough bug-fixes and features have been implemented. We may also define certain features to be included for a new release, and if there are significant changes to the engine -- let's say the implementation of multiplayer in OpenMW -- we will release a major version, leading us to OpenMW 2.0, 3.0, 4.0 etc. But it is also possible that we will have a 1.215.0 release without the need of a major version update.

%
%--- Hold on a moment! Do you plan to add any content to the original game?
%

Q:
Hold on a moment! Do you plan to add any content to the original game? X

A:
The OpenMW project is not a modding project like, e.g., Tamriel Rebuilt or Morrowind Rebirth. We only provide the environment and tools to develop new mods or new games running on OpenMW. We will neither distribute any original Morrowind content nor any modifications to the vanilla game.
	
That said, there are certain exceptions: We provide several improvements to the original engine out of the box, e.g., a redesigned Alchemy window or the new water shader. The word content in general refers to everything accomplishable with our editor, OpenMW-CS, or pure replacers, e.g., texture or animation replacers.

%
%--- I like vanilla Morrowind quite a bit. Will I be able to play Morrowind true to the original game in the future? X
%

Q:
I like vanilla Morrowind quite a bit. Will I be able to play Morrowind true to the original game in the
future?

A:
The OpenMW team sets a high value on backward compatibility, i.e., you will be able to enjoy the original Morrowind experience even on, let's say, OpenMW version 4.0 -- of course, with several improvements like fewer bugs, fewer crashes, better performance, and the above-mentioned Alchemy window or water shader.

%
%--- What can modders expect from OpenMW and OpenMW-CS?
%

Q:
What can modders expect from OpenMW and OpenMW-CS? X

A:
The The Elder Scrolls series has always had a big modding community. We want to allow modders to change nearly every aspect of the original game, and of all future games powered by the OpenMW engine. In order to achieve this, we will de-hard-code most of the game mechanics and variables. Please note that this step will take some time and has to be divided into several intermediate steps in order to keep full backward compatibility with the original game and ensure a clean implementation.
	
In addition, we plan to build up OpenMW-CS to be an even more powerful editor than it is today, making it a superior tool to create new content for OpenMW. Modders can expect OpenMW-CS to provide every functionality of the original Construction Set, plus many more, while being easy to use and highly customisable, allowing for a much faster workflow.

%
%--- Do game developers have to start their games from scratch?
%

Q:
Do game developers have to start their games from scratch? X

A:
No, we will provide a so-called game template which allows developers to start their work with the minimum of assets needed to run OpenMW. This will also include some extras to save them some time in the beginning. So far, the OpenMW Game Template will contain basic animation and terrain files as well as basic scripts.

%
%--- Give me multiplayer!
%

Q:
I kindly ask you to implement multiplayer in Morrowind. X

A:
Since OpenMW is an open source project, multiplayer is well within the scope of our engine. However, this is no trivial task, and the OpenMW team itself concentrates on our version-1.0 goals. Nevertheless, a group of developers and Morrowind enthusiasts has come together to form TES3MP, a project with the goal of bringing multiplayer to OpenMW. It basically is a fork of OpenMW, sharing most of its source code. The team has recently announced version 0.2.0 which improves the save and the load system.
	
Please note that it may take a long time until actual co-op play-throughs with your best friend or large PvP arena battles will be possible in OpenMW. But it looks like Morrowind multiplayer will be ready -- sooner or later. You can find the links to the project's page and resources below this video.

%
%--- Is OpenMW able to run the Xbox version of Morrowind?
%

Q:
Is OpenMW able to run the Xbox version of Morrowind? X

A:
No, the Xbox version is not supported in OpenMW. Bethesda asked us not to mix platforms, meaning that the Xbox version is only playable on the Xbox or the Xbox 360.

%
%--- Does OpenMW support gamepads?
%

Q:
Does OpenMW support gamepads? X

A:
OpenMW already provides basic support for gamepads. That said, user reports indicate that the actual implementation needs improvement, above all, we are currently lacking a proper gamepad user interface. If there's anyone willing to tackle this task, OpenMW will definitely include full gamepad support in the future.

%
%--- What about Virtual Reality systems like Oculus Rift? Will they be supported?
%

Q:
What about Virtual Reality systems like Oculus Rift or OpenVR? Will they be supported? X

A:
OpenMW does not yet support VR systems. In general, it would be possible to integrate them into our engine in the future. This would need some changes to the user interface or the internal controls -- but nothing that would stop a talented coder from implementing it. However, there are certain limitations to the systems we can support. OpenMW is open source and licensed under the GPL which makes it incompatible with closed-source software like OpenVR. Also, the OpenMW team wants their engine to be cross-platform which currently excludes Oculus Rift. Maybe OSVR would be an option here -- but do not expect VR support to be included before version 1.0 or any time soon.

%
%--- Is it possible to play Morrowind on Android using OpenMW?
%

Q:
Is it possible to play Morrowind on Android using OpenMW? X

A:
There has been some excitement about an Android port of Morrowind via OpenMW which also raised Bethesda's attention in a negative way. Like I said before, OpenMW is an engine for the PC version of Morrowind. That doesn't mean OpenMW is restricted to the PC but that Morrowind playability is. We agreed with Bethesda on not to advertise OpenMW on other platforms using Morrowind contents.
	
That said, there is an early version of an Android port which is sporadically updated and free to use for everyone. Please let me repeat: Whatever you are doing with the code, please do not advertise Morrowind on other platforms than the PC. The OpenMW team will not support such actions at all!

%
%--- What about other platform support in general?
%

Q:
What about other platform support in general? X

A:
The primary target platforms of OpenMW are Windows, Linux, and Mac OSX. However, OpenMW is platform-agnostic and supports devices running OpenGL 2.0 or higher. Check out our written FAQ for more information. Keep in mind that platform support always depends on people implementing and maintaining it.

%
%--- Why does OpenMW not support all TES games?
%

Q:
Why does OpenMW not support all TES games? X

A:
Although the last three The Elder Scrolls games are based on the same engine, Bethesda made many changes to adapt their engine to modern hardware, better graphics and animation, and new game mechanics. Directly using OpenMW with Oblivion's or Skyrim's assets would result in absolute chaos -- not to mention the legal implications as Bethesda doesn't want anyone to mix up their game content. Nevertheless, it would be possible to take OpenMW as a basis, adjust the code and release OpenOB or OpenSK. I don't need to stress the fact that this would take a huge amount of time and manpower. Unfortunately, there is no jack-of-all-trades engine to run all TES games.

%
%--- Outro
%

I hope that most questions regarding future development of OpenMW have been answered by now. If you have any further questions, put them in the comments below, or visit our forums at openmw.org.

In the next video, we will deal with the subject of mod compatibility. Until then, thanks for watching!



***



%-----------------------------------------------%
%----- Part 4: Morrowind Mod Compatibility -----%
%-----------------------------------------------%

%
%--- Intro
%

Hello! You've finally made it to part four of our FAQ series. This time, we need to talk about OpenMW's compatibility with user-created content aka mods which were created for the original Morrowind engine. Let's jump right in.

%
%--- I use some mods with Morrowind. Well, dozens, to be honest. Will they play on OpenMW?
%

Q:
I use some mods with Morrowind. Well, dozens, to be honest. Will they play on OpenMW? X

A:
OpenMW should support all Morrowind mods which match the following criteria:
	
	1. The mod is a pure replacer or a modification created with the original Construction Set.
	2. The mod does not rely on third-party software or other engine hacks.
	3. The mod does not contain any severe scripting errors or other crucial design flaws.
	
These requirements are necessary in order to keep OpenMW's code clean of workarounds, fall-backs, forgiving error-checking methods, and duplicated functions. We have implemented a decent amount of such things though to cope with the most common modding sins, especially with scripting errors. The original engine virtually encouraged modders to make mistakes in their code.
	
While the majority of mods meet our requirements, there are a significant number of mods which are completely or partially incompatible with OpenMW. These mods either must be rewritten or must hope for future workarounds to be implemented in OpenMW's source code.

%
%--- I have installed a normal mod which works in the vanilla game but not in OpenMW. What's wrong there?
%

Q:
I have installed a normal mod which works in the vanilla game but not in OpenMW. What's wrong there? X

A:
On the one hand, there could be errors in the mod itself. OpenMW is more verbose when it comes to design flaws or incorrect script code.
	
On the other hand, there also are many unknown bugs in OpenMW which do not appear in a normal play-through without mods. More and more users start their game with heavily modded setups, making these issues more obvious. This way, we get an increasing amount of complaints about perfectly normal mods -- which, in turn, allows us to fix our engine's behaviour.
	
If you have problems with a particular mod and think the error is on OpenMW's side, please check out our Mod Compatibility Wiki page, contact us in our Mod Compatibility subforum, or file a bug report on our bug tracker.

%
%--- I checked your Mod Compatibility page but it only includes a small number of mods. Where is the rest?
%

Q:
I checked your Mod Compatibility page but it only includes a small number of mods. Where is the rest? X

A:
Our recently reworked Wiki page collects the results of selective mod-testing done by our forum members. It does not claim to be complete -- and most likely it will never contain all Morrowind mods. It is just a list of mods which happen to be tested. Please note that most of these mods where tested without any other active mods. Thus, the information on this page is limited to single-mod setups on OpenMW.
	
Feel free to use our forums to give us feedback about mods you are using, or even assign yourself to our Wiki group, if you are already a forum member. New mod testers are always welcome!

%
%--- What about utilities like MWSE, MGE XE, or MCP? Will OpenMW never support these?
%

Q:
What about utilities like MWSE, MGE XE, or MCP? Will OpenMW never support these? X

A:
There are several utilities which expand the original engine's capabilities. The most famous of them are the Morrowind Script Extender (MWSE), the Morrowind Graphics Extender (MGE and MGE XE respectively), and the Morrowind Code Patch (MCP). All of them are third-party software which is not supposed to work with OpenMW. The same goes for mods which use the extended scripting or graphical features of these utilities.
	
That's the bad news. The good news is that OpenMW will provide most of the abovementioned functionality right from the start in the future. Additional script functions and graphical features will be implemented, and some of MGE's features have already made their way into our engine. However, we may skip a few features here and there because of unnecessarily complex implementation or security risks. To be clear: OpenMW will never support any of the third-party utilities or any of the mods depending on them. Instead, we will allow modders to achieve an ever better functionality without dirty engine hacks. -- And, with all due respect for the hard-working mod authors, I think this is the best solution since many mods are a collection of workarounds for the original engine's limitations, limitations OpenMW will never have in the first place.

%
%--- What about MGSO?
%

Q:
And what about MGSO? X

A:
The Morrowind Overhaul: Graphics and Sounds is one of the biggest mod compilations ever made. As it integrates all the abovementioned third-party utilities, it is not compatible with OpenMW. However, you are free to install MGSO for your vanilla installation, import the data files and mod list to your OpenMW installation and hope for the best. Chances are that your OpenMW-nised MGSO only has minor issues, e.g., shiny textures because of fake bump maps on textures being displayed the right way. It is also most likely that parts of mods are missing because they rely on functionality which is not present in OpenMW.
	
A general advice for all mod users: OpenMW rarely crashes because of incompatibility issues. You are more likely to find a whole bunch of warning messages in your console -- these often give a clue about what is missing or what function is failing to execute.

%
%--- How does OpenMW handle the load order of my setup?
%

Q:
How does OpenMW handle the load order of my setup? X

A:
The original Morrowind engine uses the files' time stamps to determine the load order. This creates various problems when using mods which need to be in a particular order. There are several utilities which serve the purpose to manually adjust the load order. All of this is unnecessary in OpenMW. You can simply adjust the load order of your game files by drag and drop in our launcher, while our software indicates every file's dependencies. To be honest, there should be more functionality added to our launcher in the future but the system itself works fine.

%
%--- What about registering .bsa archives in OpenMW?
%

Q:
What about registering .bsa archives in OpenMW? X

A:
.bsa archives are files which contain an arbitrary amount of game assets, e.g., textures, meshes, or sound files. To make the original engine use the files inside such an archive, you have to register the archive itself via an external tool. You can also extract all included assets to the Data Files directory but this method is not suitable for more complex setups or quickly switching mods on and off. OpenMW doesn't need any external program to register .bsa files. You can simply add them as a so-called fall-back archive in the openmw configuration file. In the future, the OpenMW team wants to abandon the use of .bsa archives for newly created content in favour of OpenMW's capability to use multiple data paths.

%
%--- Outro
%

That's all I wanted to say about mod compatibility and mod setups. If you have any further questions, please ask them in the comments section below, or visit our forums on openmw.org.

Prepare yourself for the next video which is all about your possibilities to contribute to our project. Until then, thanks for watching!



***



%----------------------------------------------------%
%----- Part 5: How To Contribute To Our Project -----%
%----------------------------------------------------%

%
%--- Intro
%

Welcome to part five of our FAQ series. This will be the last video for now but future updates may further expand this series. Today, we will deal with one of the most important topics: How you can contribute to our OpenMW project.

%
%--- How can I contribute to your project? X
%

Q:
I'm eager to help you. How can I contribute to your project?

A:
There are many different ways to help OpenMW reach its goals. You could spread the word about our project, help us with translation tasks, test our engine, write documentation, or become a developer for OpenMW or OpenMW-CS respectively. And now for the details.
	
By the term spread the word I mean any activity which aims at increasing OpenMW's awareness level. To achieve this, you could share our release announcements, videos, and news posts on other sites or forums. Moreover, taking part in discussions on related forums or on reddit would allow us to introduce more people to OpenMW and help us to fight misconceptions about our engine. Maybe you have a friend who loves classic RPGs? Why not ask him to start his newest Morrowind play-through on OpenMW? You see, there are many ways to support our project without overcommitting yourself.
	
Another task for non-developers is translation work. Morrowind and OpenMW have an international community, and not everyone has sufficient English skills to fully understand what's going on with our project or what the hell this video guy is babbling about. If you are a confident writer, why not start translating stuff into your native language? There are news posts, release announcements, Wiki pages and video subtitles waiting for you -- just introduce yourself in our Join the team subforum, include a piece of translation for an arbitrary OpenMW-related text and become a fully-fledged member of our team.
	
If you are interested in OpenMW, you are most likely familiar with Morrowind or, at least, plan to play the game using OpenMW. Since OpenMW is already playable, why not start your Morrowind play-through equipped with pen and paper in order to note anything which appears strange to you. You could also have our bug tracker and forums opened to quickly search for any issues you've encountered during your hours of gaming. When you've found a so far unknown bug, feel free to ask about it in our forums -- or directly file a bug report on our bug tracker. In the latter case, please note our bug report guidelines.
	
Apart from testing the vanilla game, we also need people who test Morrowind mods on OpenMW. Remember that one of our main goals is full support of existing mods given they were created in a clean way. We already have a Mod Status Wiki page to keep track of tested mods but there are thousands of modifications which need to be tested. You could either do so by simply using a modded setup for your play-through or by selectively testing particular mods on a clean OpenMW installation. Again, please report your findings in our forums -- this time those for mod compatibility --, file bug reports if necessary and don't forget to update our Wiki page. Every forum member can assign him- or herself to the Wiki group in order to gain access to the full editing options.
	
Another mod-related task is our OpenMW-CS user manual. We want to provide modders a compendium of the editor's features, including definitions of all technical terms, an explanation of associated files and directories, and general tutorials on creating content with OpenMW-CS. If you are interested in modding, want to become familiar with our editor, or want to help other modders finding their way in our editor, this is your opportunity to become part of the OpenMW project.

All of the aforementioned tasks do not involve any specific coding skills. But if we want to release version 1.0 within a reasonable time frame, we need more help on the coding side of things. Unfortunately, the majority of tasks left to do before version 1.0 will be released is about bug-fixing and optimisation. To make things even worse, most of these tasks are only suitable for advanced coders familiar with OpenMW's source code. So, if you have any C++ experience but don't feel confident about writing code for our project, you are welcome to help us with code documentation. This would not only make it easier for other developers to work on our engine but also help you acquainting yourself with OpenMW's structure in order to become part of our development team later.
	
Which leads to the crucial point of this video: We are in dire need of active developers for OpenMW. Our current engine development team consists of two regular and a few sporadic contributors. That may be enough to fix some bugs here and there but it is not enough to implement new features or quickly react to new issues on our bug tracker. So if you are experienced with C++ and already are familiar with our source code or are willing to spend some time digging through it, you are very welcome to join our development team. This may not be the most exciting or rewarding time for developers but you could be among the team members who helped OpenMW take the last steps towards version 1.0. Let us finish this business as soon as possible and then enjoy the coding freedom of the post-1.0 era!
	
An important part of OpenMW is our home-brew editor, OpenMW-CS. It will be the heart of future Morrowind modding and the starting point for any new game developed for OpenMW. Accordingly, my plea for new developers also applies to OpenMW-CS. Maybe you have got some skills in QT and some spare time to help us on the editor's side? Similar to the actual engine, we need to do the basic work before we can further expand OpenMW-CS's functionality and make it the world builder it is meant to be. Join our efforts and start coding in remembrance of all the Morrowind modders who suffered from the original Construction Set!

%
%--- Are there any further tasks which are not directly bound to OpenMW?
%

Q:
Phew, many things do to. Are there any further tasks which are not directly bound to OpenMW? X

A:
Well, there are two more tasks indeed: The OpenMW Game Template project and the OpenMW Example Suite.
	
The first one is the attempt to provide game developers the minimum of assets they need in order to run OpenMW without any Morrowind dependency. This way, they will be able to create their personal assets and develop their contents in OpenMW-CS, while at the same time being able to test everything in OpenMW. The link to the corresponding forum thread can be found below this video.
	
The second project is kind of a showcase world. OpenMW already has some features that weren't supported by the original engine -- and the Example Suite is the place where these features can be presented. Its main goal is to provide a world distinct from Bethesda's Nirn, including a small story-line, new items, new NPCs, and new quests in order to let players explore OpenMW's capabilities along the way.
	
Both projects are in active but rather slow development, and especially the Example Suite lacks many contents. So all you creative people out there: You may not be an experienced coder or a patient game tester, but why not turn your passion into work? We need new meshes and textures, icons, splash screens, maybe concept art. And not to forget all the texts that need to be written: dialogues, journal notes, item and spell descriptions, or new in-game books. Come to our Example Suite subforums and ask what you can do!

%
%--- Is there a possibility to help you out in a financial way?
%

Q:
Is there a possibility to help you out in a financial way? X

A:
The OpenMW team does not accept any donations to our project. That said, it is perfectly fine to support certain developers but not on behalf of the OpenMW team. There is a Patreon account of our developer scrawl but it is on hiatus for the time being. Another Patreon account was created by DestinedToDie, our current Example Suite project leader.
	
In the future, an option would be to make contracts for individual feature requests, e.g., via Bountysource.com. This way, coders would be able to pick a task, submit their solution and get the monetary reward on success. However, such a step will not be taken without a fundamental debate about partially monetising OpenMW's development. We are a hobbyist project after all!

%
%--- Outro
%

Congratulations, you have finally reached the end of this FAQ series. If there are any more questions about how to help our project, put them below this video, or visit our forums on openmw.org.

See you soon for the 0.41 release commentary, thanks in advance for your help -- and, as always, thanks for watching!