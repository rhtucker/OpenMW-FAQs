# OpenMW FAQ

**Table of Contents**

- [General](#general)
    - [What exactly *is* OpenMW?](#what-exactly-is-openmw)
    - [So what is a game engine?](#so-what-is-a-game-engine)
    - [So what is OpenMW-CS and how does it fit in?](#so-what-is-openmw-cs-and-how-does-it-fit-in)
    - [Why would you reimplement a *whole* game engine?](#why-would-you-reimplement-a-whole-game-engine)
    - [Do I need a copy of Morrowind to use OpenMW if it's just an engine?](#do-i-need-a-copy-of-morrowind-to-use-openmw-if-its-just-an-engine)
    - [Can I convert my Morrowind saves to be playable on OpenMW?](#can-i-convert-my-morrowind-saves-to-be-playable-on-openmw)
    - [All I want is ______. Will OpenMW benefit *me*?](#all-i-want-is-______-will-openmw-benefit-me)
    - [I thought this was a modern engine, why do the models and textures still look so bad!?](#i-thought-this-was-a-modern-engine-why-do-the-models-and-textures-still-look-so-bad)
- [Installation](#installation)
    - [How do I install OpenMW?](#how-do-i-install-openmw)
    - [How do I install Morrowind if I'm not on Windows?](#how-do-i-install-morrowind-if-im-not-on-windows)
    - [How do I update OpenMW from an older version?](#how-do-i-update-openmw-from-an-older-version)
    - [Can OpenMW be installed on Android or Raspberry Pi?](#can-openmw-be-installed-on-android-or-raspberry-pi)
    - [How do I install a game besides Morrowind?](#how-do-i-install-a-game-besides-morrowind)
    - [Can I install another Elder Scrolls game like Skyrim on OpenMW?](#can-i-install-another-elder-scrolls-game-like-skyrim-on-openmw)
    - [Can I use the assets from the Xbox version of Morrowind?](#can-i-use-the-assets-from-the-xbox-version-of-morrowind)
- [Modding](#modding)
    - [What advantages does OpenMW and OpenMW-CS currently offer modders?](#what-advantages-does-openmw-and-openmw-cs-currently-offer-modders)
    - [How do I install mods in OpenMW?](#how-do-i-install-mods-in-openmw)
    - [How does OpenMW handle load order?](#how-does-openmw-handle-load-order)
    - [What do I need to do to register .bsa archives in OpenMW?](#what-do-i-need-to-do-to-register-bsa-archives-in-openmw)
    - [Can I install MWSE/MSE-XE/MCP with OpenMW?](#can-i-install-mwsemse-xemcp-with-openmw)
    - [I have mods that require MWSE/MSE-XE/MCP. Will they be compatible?](#i-have-mods-that-require-mwsemse-xemcp-will-they-be-compatible)
    - [I have mods that work with vanilla Morrowind. Will they be compatible?](#i-have-mods-that-work-with-vanilla-morrowind-will-they-be-compatible)
    - [What content does OpenMW add to the original game?](#what-content-does-openmw-add-to-the-original-game)
    - [Can I develop a new game with OpenMW/OpenMW-CS?](#can-i-develop-a-new-game-with-openmwopenmw-cs)
    - [Can I sell a game/mod I develop for OpenMW?](#can-i-sell-a-gamemod-i-develop-for-openmw)
- [Project Status](#project-status)
    - [What are the goals for OpenMW version 1.0?](#what-are-the-goals-for-openmw-version-10)
    - [When will version 1.0 be released?](#when-will-version-10-be-released)
    - [What are the goals beyond that (post-1.0)?](#what-are-the-goals-beyond-that-post-10)
    - [That's the future, what can I expect from OpenMW *right now*?](#thats-the-future-what-can-i-expect-from-openmw-right-now)
    - [Will the main branch of OpenMW support multiplayer?](#will-the-main-branch-of-openmw-support-multiplayer)
    - [What's the status of ______ feature?](#whats-the-status-of-______-feature)
    - [How is OpenMW close to version 1.0 when we're only on version 0.4x?](#how-is-openmw-close-to-version-10-when-were-only-on-version-04x)
    - [I don't care about Morrowind, can I create my own game *right now*?](#i-dont-care-about-morrowind-can-i-create-my-own-game-right-now)
- [Contribute to the Project](#contribute-to-the-project)
    - [How can I help contribute to OpenMW?](#how-can-i-help-contribute-to-openmw)
    - [Is there anything I can help with that doesn't involve programming?](#is-there-anything-i-can-help-with-that-doesnt-involve-programming)
    - [How do I report bugs?](#how-do-i-report-bugs)
    - [Can I support the project monetarily?](#can-i-support-the-project-monetarily)
- [Technical Questions](#technical-questions)
    - [What are OpenMW's hardware requirements?](#what-are-openmws-hardware-requirements)
    - [Isn't rebuilding a commercial game illegal?](#isnt-rebuilding-a-commercial-game-illegal)
    - [What license is OpenMW released under?](#what-license-is-openmw-released-under)
    - [So can I develop and sell a game based on the OpenMW engine?](#so-can-i-develop-and-sell-a-game-based-on-the-openmw-engine)
    - [What technologies does OpenMW use?](#what-technologies-does-openmw-use)
    - [When does the S3TC patent expire?](#when-does-the-s3tc-patent-expire)
- [Troubleshooting](#troubleshooting)
    - [I can launch OpenMW, and music plays, but I only see a black screen.](#i-can-launch-openmw-and-music-plays-but-i-only-see-a-black-screen)
    - [Why do some of the rocks and other textures look shiny?]

***

## General

### What exactly *is* OpenMW?

OpenMW (open em double-yu) is a free, open-source game engine that reimplements, and extends beyond, the 2002 Gamebryo engine for *The Elder Scrolls III: Morrowind* by Bethesda Softworks. While bringing modern technology to *Morrowind* has always been a large part of our mission, our overall goal is to create a general-purpose, 1st/3rd-person, real-time, role-playing game engine. Eventually this will mean anyone can use our editor, OpenMW-CS (open em double-yu see ess), to create a full game from scratch, and users will be able to play said game in OpenMW.

Both OpenMW and OpenMW-CS are written from scratch and aren’t made to support any third-party programs players use to improve the original *Morrowind* engine. However, we do seek to implement much of the functionality of several of the popular third-party tools.

OpenMW is not a "mod" to Morrowind. It is an entirely different game engine that does not use the original Morrowind.exe in any way.

### So what is a game engine?

A game engine, according to [Wikipedia](https://en.wikipedia.org/wiki/Game_engine), is "a software development environment designed for people to build video games. Developers use them to create games for consoles, mobile devices, and personal computers. The core functionality typically provided by a game engine includes a rendering engine ("renderer") for 2D or 3D graphics, a physics engine or collision detection (and collision response), sound, scripting, animation, artificial intelligence, networking, streaming, memory management, threading, localization support, scene graph, and may include video support for cinematics."

Basically, it's the framework that developers work within to speed up the creation of a game. If all of those basic functions had to be recreated every time a new game was created, our games would take significantly more time and cost a lot more money. This allows game developers to focus on actual content creation, like writing the story, creating the character and environment models, skinning them with textures, writing custom scripts, and a huge number of other things. In the case of *Morrowind*, the ESM/ESP files along with BSAs are the content that Morrowind.exe reads to actually run the game.

### So what is OpenMW-CS and how does it fit in?

OpenMW-CS is OpenMW's answer to the Construction Set that came with the original *Morrowind*. It is a powerful tool that allows users to easily create modifications, or mods, for their game. OpenMW-CS extends beyond this, and allows users to create entirely new games, not dependent on Bethesda's intellectual property.

Unlike OpenMW itself, OpenMW-CS does not seek to perfectly recreate the original experience. Instead we hope to improve on the original's good parts, while dropping the bad and ugly points and adding additional features and tools to make it more a tool made by modders for modders. Some of the improvements over the original include:

- non-blocking (what does this mean???)
- multi-threaded
- multi-document support
- multi-view support
- high scalability
- customisable GUI

Significantly, during the dehardcoding process for the project post-1.0, many things that couldn't be changed in *Morrowind* will be trivial to modify in OpenMW-CS, greatly expanding the scope of possible mods. While we seek to keep the editor fully up-to-date with the corresponding OpenMW version, the current state of OpenMW-CS lags significantly behind. We're aiming to catch up by OpenMW 1.0, but for the time being several essential features are missing in the editor. Once this is achieved however, OpenMW-CS will allow users to edit any newly implemented features!

### Why would you reimplement a *whole* game engine?

Essentially, *Morrowind*'s original engine has its flaws. There are many bugs which will never be fixed considering the game's age. Moreover, it tends to be unstable and was optimised for, very outdated hardware by today's standards. Also, the modding capabilities are limited when it comes to changes in the actual game mechanics, which lead to a variety of hackish third-party software to overcome at least some of the hard-coded limitations. Perhaps more importantly, the vanilla engine only runs natively on Windows, ignoring a growing part of the gaming community. Furthermore, the original engine will never allow the flexability necessary to create truly independent new games.

While popular third-party software has allowed incredible improvements over the original *Morrowind* engine in recent years, they are not fundamentally the ideal way of dealing with the engine's limitations. The OpenMW project has not yet caught up to the combined scope of these tools, but we eventually hope to enable users and modders a more streamlined and efficient way of playing, editing, and creating new content while enjoying the delights of modern technology.

### Do I need a copy of Morrowind to use OpenMW if it's just an engine? What else can I play?

The answer depends on what you're trying to do.

If you want to play Morrowind, then yes. Since we are legally unable to provide any Morrowind assets (BSAs, ESMs, textures, sounds, etc.), you must provide your own copy of the game and its content. The Morrowind Game of the Year Edition is readily available on a variety of sites, generally for very cheap, and regularly on sale. It can be installed from an electronic copy or from a CD copy, but OpenMW cannot use the [Xbox version](#can-i-use-the-assets-from-the-xbox-version-of-morrowind).

If you're trying to play a different game, you won't need Morrowind at all. There are no current plans to [support other Bethesda games](#can-i-install-another-elder-scrolls-game-like-skyrim-on-openmw) (one thing at a time!), but post-1.0 it will be possible to create entirely new games using OpenMW-CS. Any of these games will be playable in OpenMW. See the <link to example suite> to see what this may be like!

### Can I convert my Morrowind saves to be playable on OpenMW?

The short answer is no. Some [experiments](https://forum.openmw.org/viewtopic.php?t=3208) exist, but since the original save format isn't documented, this has mostly been abandoned due to the extreme difficulty of working with them. The many bugs that crop up are likely to render your save useless, so it is not recommended. We understand this can be frustrating for users who have invested tens or hundreds of hours in their game, but Morrowind is a huge game with tons of replayability. Maybe it's time you started a new playthough! OpenMW can be installed alongside Vanilla Morrowind, so you can always go back to your old game if you like. Hopefully someone picks up further development of this tool in the future, but for now, use at your own peril.

### All I want is ______. Will OpenMW benefit *me*?

This is a huge question, and the answer depends on the fill in the blank. Apart from simply being the most stable way of running Morrowind, we've compiled <this> list of popular features and what their current and expected future statuses are. We understand if you're skeptical of using OpenMW until it fully supports your favorite feature, but we strongly urge you to try it out anyway to see how you like it. We also urge you to check back every few months and see what progress has been made on your pet feature! If you simply can't wait for a feature, you can always become a contributor and join the ranks of our developers to help build the feature for others to enjoy!

### I thought this was a modern engine, why does stuff still look so bad!?

We're working on new graphical features all the time, but we will always be limited to a degree by the "Vanilla" game assets. Morrowind was released in 2002, and its content reflects the hardware available during that time. The OpenMW project scope does not include updating Morrowind content for both practical and legal reasons. Upgrading the meshes and textures to more visually pleasing versions will have to be done by modders, but luckily, all current mesh/texture mods are compatible! The one exception are mods made using the "fake" bump-mapped features available in MGE. More information on this and how it can be fixed is [here](https://openmw.readthedocs.io/en/master/reference/modding/texture-modding/convert-bump-mapped-mods.html#).

***

## Installation


### How do I install OpenMW?

Installation of OpenMW is very simple. First you must download the binary for your operating system from [GitHub](https://github.com/OpenMW/openmw/releases). Then simply run the installer and follow the prompts. The OpenMW engine is now installed!

For more detailed instructions or for other operating systems, please see our [Official Documentation](https://openmw.readthedocs.io/en/latest/manuals/installation/install-openmw.html).

### How do I install Morrowind if I'm not on Windows?

### How do I update OpenMW from an older version?

### Can OpenMW be installed on Android or Raspberry Pi?

You can run the OpenMW-Template right from the start on devices that support at least OpenGL 2.0 with ARVv7 and above.

This means OpenMW can run on a Raspberry Pi 2 provided that VC4 kernel module and userland support is there. It should be mainlined into Linux kernel 4.5 and available in Xorg as of 11.1. OpenMW will compile on Raspberry Pi B+ or earlier, getting it running will be more difficult. Feel free to experiment.

### How do I install a game besides Morrowind?

### Can I install another Elder Scrolls game like Skyrim on OpenMW?

Currently, no. The amount of work it would take to streamline the installation process as we have done for Morrowind is far out of our scope. The possibility does exist in the future however.

### Can I use the assets from the Xbox version of Morrowind?

No, there is currently no support for the Xbox version of Morrowind. Bethesda has explicitly asked us not to mix platforms in this regard, so there will be no Xbox Morrowind on the PC or any other non-Xbox platform.

***

## Modding

### What advantages does OpenMW and OpenMW-CS currently offer modders?

### How do I install mods in OpenMW?

### How does OpenMW handle load order?

### What do I need to do to register .bsa archives in OpenMW?

### Can I install MWSE/MSE-XE/MCP with OpenMW?

### I have mods that require MWSE/MSE-XE/MCP. Will they be compatible?

### I have mods that work with vanilla Morrowind. Will they be compatible?

### What content does OpenMW add to the original game?

### Can I develop a new game with OpenMW/OpenMW-CS?

There are still [a number of issues](https://gitlab.com/OpenMW/openmw/issues?label_name%5B%5D=1.0) that need to be addressed before we believe that we have reached parity with the original Morrowind game engine and Construction Set. In particular, many of the new features we are planning for post-1.0 will require a functioning editor so that we can create mods that use them, thus bringing OpenMW-CS to parity is very important.

The best way to make sure that OpenMW and OpenMW-CS reach version 1.0 faster is to [help the project](https://openmw.org/faq/#help) however you can. We can certainly use your help writing code, testing, or generally just providing feedback.

### Can I sell a game/mod I develop for OpenMW?



***

## Project Status

### What are the goals for OpenMW version 1.0?

### When will version 1.0 be released?

### What are the goals beyond that (post-1.0)?

### That's the future, what can I expect from OpenMW *right now*?

### Will the main branch of OpenMW support multiplayer?

### What's the status of ______ feature?

### How is OpenMW close to version 1.0 when we're only on version 0.4x?

### I don't care about Morrowind, can I create my own game *right now*?


***

## Contribute to the Project

### How can I help contribute to OpenMW?

### Is there anything I can help with that doesn't involve programming?

### How do I report bugs?

### Can I support the project monetarily?

***

## Technical Questions

### What are OpenMW's hardware requirements?

### Isn't rebuilding a commercial game illegal?

### What license is OpenMW released under?

### So can I develop and sell a game based on the OpenMW engine?

### What technologies does OpenMW use?

### When does the S3TC patent expire? 

***

## Troubleshooting

### I can launch OpenMW, and music plays, but I only see a black screen.
